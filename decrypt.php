<?php
  require_once 'db/common.php';

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // get the session, and make sure it's all good
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);
  dieIfNotPhase($session, PHASE_PLAYING);
  
  // team details
  $myteam = getPlayerTeam($sessionid, $playerid);
  $myteamname = teamName($myteam);
  $mystate = $session["${myteamname}state"];
  $myencryptor = $session["${myteamname}encryptor"];

  // check we are in right state and are the right player
  if ($mystate != STATE_DECRYPTING) error("game is not in correct state.");
  if ($myencryptor == $playerid) error("the encryptor cannot do this.");

  $guesses = array();
  for ($index=1; $index<=GUESS_COUNT; $index++) {
    $guess = $_POST["decrypt_$index"];
    array_push($guesses, intval($guess));
  }

  $guessesjson = json_encode($guesses);

  // record the decryption attempt and update the game state.
  submitDecryption($session, $myteam, $guessesjson);

  // skip the intercept state if there isn't enough message
  // history to support it.
  if (!enoughToIntercept($sessionid)) {
     skipToResults($sessionid, $myteam);
     if (bothTeamsInResults($sessionid)) {
       newRound($sessionid);
     }
  }

?>
<html>
   <head>
      <title>Decryptio</title>
      <meta http-equiv = 'refresh' content = '0; url = .?<?=$_SERVER['QUERY_STRING']?>' />
   </head>
   <body>
      <p>Decrypting message...</p>
   </body>
</html>