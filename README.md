# Decryptio

This is based on the code from Dilemmio.

This is all PHP, JS using only jQuery and Bootstrap 4 (CSS only). Dump the files on your 
PHP server and it should just work. If you want to create a fresh database, /db/makedb.php 
will do that.

Most of the interesting code is in /db/common.php. /game.php renders most of the game 
interface.

No sockets or anything - pages that aren't waiting for user input do an ajax request at a 
set interval to decide when they should refresh the whole page. Only the waiting room does 
ajax-based page updates.

It's recommended you password protect the /db/ directory. I use .htaccess to do this. The 
database is SQLite. If you want to do any manual stuff with the database, phpliteadmin is 
useful.


## Logic
This is a rough state diagram for the game logic.
![Dilemmio States](/StateDiagram.png)

Mermaid syntax:
```
stateDiagram
  state fork_state <<fork>>

  [*] --> JoinSession
  JoinSession --> WaitingRoom
  JoinSession --> CreateSession
  CreateSession --> WaitingRoom: as admin
  WaitingRoom --> fork_state: admin starts game

  fork_state --> RedEncrypting
  RedEncrypting --> RedDecrypting
  RedDecrypting --> RedIntercepting
  RedIntercepting --> Results

  fork_state --> BlueEncrypting
  BlueEncrypting --> BlueDecrypting
  BlueDecrypting --> BlueIntercepting
  BlueIntercepting --> Results

  Results --> fork_state
  Results --> [*]
```
