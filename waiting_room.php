<?php
  require_once 'db/common.php';

  $playerid = $_REQUEST['player'];
  $sessionid = $_REQUEST['session'];
  $pass = $_REQUEST['pass'];

  // make sure we have a valid session
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);

  // get a list of players
  $players = getPlayerNamesInSession($sessionid);
  $sessionname = $session['name'];

  // check if you are the admin
  $isAdmin = isAdmin($session, $_REQUEST['player']);
  $startButton = ($isAdmin && count($players)>1);
?>

<html>
  <head>
    <?php htmlHead(); ?>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Decryptio</title>
  </head>
  <body>
    <div class="container">
      <h2 style="text-align:center;">'<?=$sessionname?>' waiting room</h2>
      <ul class="list-group" id="playerList">
      </ul>
      <div id="buttonspot"></div>
    </div>
  <script type="text/javascript">

    var teamsCurrentlyValid = false;

    function makeButton() {
      if ($("#startbutton").length === 0) {
        $("#buttonspot").html('<a class="btn btn-primary" id="startbutton" href="start_game.php?<?=$_SERVER['QUERY_STRING']?>" role="button" onclick="return confirmButton();">Start Game</a>');
      }
    }

    function confirmButton() {
      if (teamsCurrentlyValid)
        return confirm('Start this game and close the waiting room?');
      alert('You need at least two players on each team.');
      return false;
    }

    function setTeam(id, team) {
      $.getJSON( `ajax.php?req=setteam&chplayer=${id}&team=${team}&<?=$_SERVER['QUERY_STRING']?>`, function( data ) {
        if (data.response === 'team_set') {
          console.log('player team changed');
          restartInterval();
        } else {
          console.error('problem updating teams');
        }
      });
    }
    function setRed(id) { return setTeam(id, <?=TEAM_RED?>); }
    function setBlue(id) { return setTeam(id, <?=TEAM_BLUE?>); }

    function makeRadioButtons(team, id) {
      <?php if ($isAdmin) { ?>
      var redact = blueact = redchk = bluechk = "";
      // using == here intentionally
      if (team == <?=TEAM_RED?>) {
        redact = "active";
        redchk = "checked";
      }
      if (team == <?=TEAM_BLUE?>) {
        blueact = "active";
        bluechk = "checked";
      }
      return `
<div class="btn-group btn-group-toggle teamselect" style="float:right;" data-toggle="buttons">
  <label class="btn btn-danger btn-redteam btn-sm ${redact}" ${redact}>
    <input type="radio" ${redchk} onclick="javascript:setRed(${id});"> Red
  </label>
  <label class="btn btn-primary btn-blueteam btn-sm ${blueact}" ${blueact}>
    <input type="radio" ${bluechk} onclick="setBlue(${id});"> Blue
  </label>
</div>
  `;
  <?php } else echo "return '';"; ?>
    }

    // regular update of the players in this session.
    function updatePlayers(players) {
      var cont = $("#playerList");
      var html = "";

      var redCount = 0;
      var blueCount = 0;

      for (var i=0; i<players.length; i++) {
        var player = players[i];
        if (player.team == <?=TEAM_RED?>) redCount++;
        if (player.team == <?=TEAM_BLUE?>) blueCount++;
        html += `<li class="list-group-item team${player.team}">${player.name}${makeRadioButtons(player.team, player.id)}</li>`;
      }

      teamsCurrentlyValid = redCount >= 2 && blueCount >= 2;

      // if we;re admin, perhaps we create the button at this point
      <?php if ($isAdmin) { ?>
        if (players.length > 1)
          makeButton();
      <?php } ?>

      cont.html(html);
    }

    // the session has changed. we need to refresh the page.
    function updateSession(session) {
      if (session.phase !== 0) {
        goToGame();
      }
    }

    function goToGame() {
      window.location.href = ".?<?=$_SERVER['QUERY_STRING']?>";
    }

    function getUpdate(returnPlayers=false){
      console.log("getting session update...");
      $.getJSON( "ajax.php?req=waiting&<?=$_SERVER['QUERY_STRING']?>", function( data ) {
        console.log(data);
        if (data.response==="players") {
          updatePlayers(data.players);
        } else if (data.response==="session") {
          updateSession(data.session);
        }
      });
    }

    var intervalHandle = 0;
    function restartInterval() {
      clearInterval(intervalHandle);
      getUpdate();
      intervalHandle = setInterval(getUpdate, <?=WAITINGROOM_INTERVAL?>);
    }

    // jquery onload
    $(function() {

      // set up regular player updates
      restartInterval();

      // makes a 'start game' button, if it doesn't exist
      <?php if ($startButton) {
        echo 'makeButton();';
      } ?>      

    });
  </script>
  </body>
</html>