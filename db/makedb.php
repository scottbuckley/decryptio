<?php
  require_once "common.php";

  function dbCreateTables() {
    $db = dbConnect();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // game sessions
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS sessions (
            id               INTEGER PRIMARY KEY AUTOINCREMENT,
            name             TEXT NOT NULL,
            password         TEXT,
            adminplayer      INTEGER NOT NULL,

            phase            INTEGER DEFAULT 0,
            round            INTEGER DEFAULT 0,

            redstate         INTEGER DEFAULT 0,
            bluestate        INTEGER DEFAULT 0,

            redencryptor     INTEGER DEFAULT -1,
            blueencryptor    INTEGER DEFAULT -1,

            redwords         TEXT,
            bluewords        TEXT,

            chcount          INTEGER DEFAULT 0,
            lastupdate       DATETIME DEFAULT CURRENT_TIMESTAMP
        );');
    $q->execute();

    // players
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS players (
            id               INTEGER PRIMARY KEY AUTOINCREMENT,
            session          INTEGER,
            team             INTEGER DEFAULT '.TEAM_RED.',
            name             TEXT NOT NULL
        );');
    $q->execute();

    // words
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS words (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            word            TEXT NOT NULL,
            collection      INTEGER DEFAULT 0
        );');
    $q->execute();

    // words
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS wordsused (
            id              INTEGER NOT NULL,
            session         INTEGER NOT NULL
        );');
    $q->execute();

    // a trigger to keep track of changes to the 'sessions' table
    $q = $db->prepare("
        CREATE TRIGGER IF NOT EXISTS 'trackChanges'
        AFTER UPDATE ON sessions
        FOR EACH ROW
        BEGIN
            UPDATE sessions
            SET lastupdate=CURRENT_TIMESTAMP,
                chcount=OLD.chcount+1
            WHERE id = OLD.id;
        END;");
    $q->execute();




    

    $db = null;
  }


  dbCreateTables();

?>