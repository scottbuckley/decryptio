<?php
  require_once 'db/common.php';

  // response will always be json
  header('Content-Type: application/json');

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  $request = $_REQUEST['req'];

  // get the session, and make sure it's all good
  $session = getSession($sessionid, $pass);
  if ($request != "sessionlist")
    dieIfInvalidSessionOrPlayer($session, $playerid, true);
  
  if ($request == 'waiting') {
    // waiting room response. this is either the current list of players,
    // or a player state if the phase has changed away from PHASE_NOTSTARTED
    if (isset($session['phase']) && $session['phase'] == PHASE_NOTSTARTED) {
      // session is still in waiting mode. return player list.
      $playerList = getPlayersInSession($sessionid);
      echo(json_encode(array("response"=>"players","players"=>$playerList)));
    } else {
      // session has progressed. send session data.
      echo(json_encode(array("response"=>"session", "session"=>$session)));
    }
  } else if ($request == 'sessionlist') {
    $sessions = getWaitingSessions();
    print_r(json_encode($sessions));
  } else if($request == 'setteam') {
    // check the player to change is in this session
    $chplayer = $_REQUEST['chplayer'];
    dieIfInvalidSessionOrPlayer($session, $chplayer, true);
    $team = $_REQUEST['team'];
    setPlayerTeam($sessionid, $chplayer, $team);
    echo(json_encode(array('response'=>'team_set')));
  } else if ($request == 'sessionchanged') {
    // checking to see if the session data has changed in any way since some point
    if ($session['chcount'] == $_REQUEST['chc']) {
      echo(json_encode(array("response"=>"same")));
    } else {
      echo(json_encode(array("response"=>"different")));
    }
  } else if ($request == 'interceptwait') {
    $myteam = $_REQUEST['team'];
    $teamname = teamName($myteam);
    if ($session["${teamname}state"] == STATE_INTERCEPTING) {
      echo(json_encode(array("response"=>"same")));
    } else {
      echo(json_encode(array("response"=>"different")));
    }
  } else if ($request == 'debug_setstate') {
    /// FOR DEBUG ONLY
    $db = dbConnect();
    $q=$db->prepare('
      UPDATE sessions
      SET state = ?
      WHERE id = ?
    ');
    $q->execute(array($_REQUEST['state'], $session['id']));
    $db = null;
    echo "done";
    return true;
  } else {
    echo json_encode(array("response"=>"invalid request type."));
  }
?>