<?php
  require_once 'db/common.php';
  $sessions = getWaitingSessions();
  $sessionCount = count($sessions);
?>
<html>
  <head>
    <? htmlHead(); ?>
    <title>Decryptio</title>
  </head>
  <body>
    <div class="container">
      <h2 style="text-align:center;">Join a session</h2>
      <form action="join_session.php" method="get">
        <div class="form-group row">
          <label for="inputName" class="col-sm-2 col-form-label">Your Name</label>
          <div class="col-sm-10">
            <input type="text" name="name" class="form-control" id="inputName" required autofocus>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputSession" class="col-sm-2 col-form-label">Session</label>
          <div class="col-sm-10">
            <select class="form-control" name="session" id="inputSession" size="<?=$sessionCount?>" required>
            <?php
              foreach($sessions as $session)
                echo "<option id=\"sess_$session[id]\" value=\"$session[id]\">$session[name]</option>";
            ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPass" class="col-sm-2 col-form-label">Session Password</label>
          <div class="col-sm-10">
            <input type="text" name="pass" class="form-control" id="inputPass">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2"></div>
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Join</button>
            <a href="new_session.php" class="btn btn-outline-primary" style="float:right;">Create a new session</a>
          </div>
        </div>
      </form>
    </div>
    <script type="text/javascript">

      function ensureSession(select, session) {
        if (select.find("#sess_"+session.id).length == 0) {
          // need to create it
          console.log("adding session to list");
          var optionHTML = `<option id="sess_${session.id}" value="${session.id}">${session.name}</option>`;
          select.append(optionHTML);
          return false;
        } else return true;
      }

      function processSessions(sessions) {
        var select = $("#inputSession");
        for (var i=0; i<sessions.length; i++) {
          ensureSession(select, sessions[i]);
        }
        if (select.attr('size') != sessions.length) {
          select.attr('size', sessions.length);
        }
      }

      function getUpdate(){
        console.log("getting session list...");
        $.getJSON( "ajax.php?req=sessionlist", function( data ) {
          console.log(data);
          processSessions(data);
        });
      }

      $(function() {
        setInterval(getUpdate, <?=PING_INTERVAL?>);
      });
    </script>
  </body>
</html>