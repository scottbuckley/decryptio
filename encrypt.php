<?php
  require_once 'db/common.php';

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // get the session, and make sure it's all good
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);
  dieIfNotPhase($session, PHASE_PLAYING);
  
  // team details
  $myteam = getPlayerTeam($sessionid, $playerid);
  $myteamname = teamName($myteam);
  $mystate = $session["${myteamname}state"];
  $myencryptor = $session["${myteamname}encryptor"];

  // check we are in right state and are the right player
  if ($mystate != STATE_ENCRYPTING) error("game is not in correct state.");
  if ($myencryptor != $playerid) error("only the encryptor can do this.");

  $clues = array();
  for ($index=1; $index<=GUESS_COUNT; $index++) {
    $clue = $_POST["encrypt_$index"];
    array_push($clues, $clue);
  }

  $cluesjson = json_encode($clues);

  submitEncryption($session, $myteam, $cluesjson);

?>
<html>
   <head>
      <title>Decryptio</title>
      <meta http-equiv = 'refresh' content = '0; url = .?<?=$_SERVER['QUERY_STRING']?>' />
   </head>
   <body>
      <p>Encrypting message...</p>
   </body>
</html>