<?php
  require_once 'db/common.php';
  $playername = $_REQUEST['name'];
  $sessionname = $_REQUEST['sessionname'];
  $sessionpass = $_REQUEST['pass'];
  
  // create the player first (not yet in a session)
  $playerid = createPlayer($playername);

  // create the session, with the new player as admin
  $sessionid = createSession($sessionname, $playerid, $sessionpass);

  // add the admin player to that session
  addPlayerToSession($playerid, $sessionid);

  $URLInfo = "?session=$sessionid&pass=$sessionpass&player=$playerid";
?>
<html>
   <head>
      <title>Session Creation</title>
      <meta http-equiv = 'refresh' content = '0; url = waiting_room.php<?=$URLInfo?>' />
   </head>
   <body>
      <p>Session created. Joining new session...</p>
   </body>
</html>