<?php
  require_once 'db/common.php';

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // get session and check it's all good
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);
  dieIfNotAdmin($session, $playerid);
  dieIfNotPhase($session, PHASE_NOTSTARTED);
  
  // choose the appropriate words
  [$redwords, $bluewords] = array_chunk(getWords(WORD_COUNT*2), WORD_COUNT);

  // start the game
  startGame($sessionid, $redwords, $bluewords);

  // set up the first round
  newRound($sessionid);

?>
<html>
   <head>
      <title>Starting session</title>
      <meta http-equiv = 'refresh' content = '0; url = .?<?=$_SERVER['QUERY_STRING']?>' />
   </head>
   <body>
      <p>Starting the session...</p>
   </body>
</html>