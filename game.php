<?php
  require_once 'db/common.php';

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // get the session, die if it's invalid
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);

  // team details
  $myteam = getPlayerTeam($sessionid, $playerid);
  $theirteam = otherTeam($myteam);
  $myteamname = teamName($myteam);
  $theirteamname = otherTeamName($myteam);

  // get some session data
  $players = getPlayerNames($sessionid);
  $playername = $players[$playerid];
  $sessionname = $session['name'];
  $sessionchc = $session['chcount'];

  // get message info


  // team-dependent info
  $mystate = $session["${myteamname}state"];
  $theirstate = $session["${theirteamname}state"];
  $mywords = json_decode($session["${theirteamname}words"]);
  $mymessage = getMessage($session, $myteam);
  $mymessageraw = json_decode($mymessage['message']);

  // encryptors
  $myencryptor = $session["${myteamname}encryptor"];
  $myencryptorname = $players[$myencryptor];
  $theirencryptor = $session["${theirteamname}encryptor"];
  $theirencryptorname = $players[$theirencryptor];

  // personal state
  $encryptorisme = ($myencryptor == $playerid);


  // defaults (will be set when rendering page)
  $refreshonchange = false;
  $interceptwait   = false;














  // HTML generator functions

  $refreshBehaviour = function() use ($sessionchc) {?>
    setInterval(function() {
      $.getJSON('ajax.php?req=sessionchanged&chc=<?=$sessionchc?>&<?=$_SERVER['QUERY_STRING']?>', function(data){
        console.log(data);
        if (data['response'] === 'different')
          location.reload();
      });
    }, <?=PING_INTERVAL?>);
  <?php };

  $interceptWaitBehaviour = function() use ($myteam) {?>
    setInterval(function() {
      $.getJSON('ajax.php?req=interceptwait&team=<?=$myteam?>&<?=$_SERVER['QUERY_STRING']?>', function(data){
        console.log(data);
        if (data['response'] === 'different')
          location.reload();
      });
    }, <?=PING_INTERVAL?>);
  <?php };

  $encryptForm = function() use ($mywords, $mymessage) {
    ?> <form action="encrypt.php?<?=$_SERVER['QUERY_STRING']?>" method="POST"> <?php
    $messagenums = json_decode($mymessage['message']);
    $index = 0;
    foreach ($messagenums as $num) {
      $word = $mywords[$num-1];
      $index++;
      ?>
        <div class="form-group row">
          <label for="inputEncrypt<?=$index?>" class="col-sm-2 col-form-label"><?=$num?>: <?=$word?></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="inputEncrypt<?=$index?>" name="encrypt_<?=$index?>" required>
          </div>
        </div>
    <?php } ?>
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Encrypt</button>
          </div>
        </div>
      </form>
    <?php
  };

  $decryptForm = function() use ($mywords, $mymessage) {
    ?> <form action="decrypt.php?<?=$_SERVER['QUERY_STRING']?>" method="POST"> <?php
    $encrypted = json_decode($mymessage['encrypted']);

    $optionshtml = "";
    $wordindex = 0;
    foreach ($mywords as $word) {
      $wordindex++;
      $optionshtml .= "<option value=\"$wordindex\">$wordindex: $word</option>";
    }

    $index = 0;
    foreach ($encrypted as $clue) {
      $index++;
      ?>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label"><?=$clue?></label>
          <div class="col-sm-10">
            <select class="form-control decryptselect" name="decrypt_<?=$index?>">
              <?=$optionshtml?>
            </select>
          </div>
        </div>
    <?php } ?>
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary" onclick="return checkDecrypt();">Decrypt</button>
          </div>
        </div>
      </form>
    <?php
  };

  $showHistory = function($history) {
    echo '<table class="table table-sm table-bordered">';
    foreach ($history as $num => $clues) {
      echo '<tr style="min-height:120px;">';
      // number
      echo "<td style=\"width:1%;\">WORD&nbsp;$num</td>";

      // clues
      echo '<td>';
      echo '<ul class="list-group list-group-horizontal lg-lesspadding">';
      foreach ($clues as $clue) {
        echo "<li class=\"list-group-item\">$clue</li>";
      } if (count($clues) == 0) {
        // if there are no clues, just put a question mark. visually more consistent, i guess.
        echo "<li class=\"list-group-item\">?</li>";
      }
      echo '</ul>';
      echo '</td>';
      echo '</tr>';
    }
    echo '</table>';
  };

  $interceptForm = function($theirencryption) {

    ?> <form action="intercept.php?<?=$_SERVER['QUERY_STRING']?>" method="POST"> <?php

    $optionshtml = "";
    for($i=1; $i<=WORD_COUNT; $i++) {
      $optionshtml .= "<option value=\"$i\">$i</option>";
    }

    $index = 0;
    foreach ($theirencryption as $clue) {
      $index++;
      ?>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label"><?=$clue?></label>
          <div class="col-sm-10">
            <select class="form-control decryptselect" name="intercept_<?=$index?>">
              <?=$optionshtml?>
            </select>
          </div>
        </div>
    <?php } ?>
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary" onclick="return checkDecrypt();">Intercept</button>
          </div>
        </div>
      </form>
    <?php
  };

  $wordList = function($indices = null) use ($mywords) {
    if ($indices == null) $indices = range(1, count($mywords));
    echo '<ul class="list-group list-group-horizontal lg-lesspadding">';
    for ($ind=0; $ind<count($indices); $ind++) {
      $number = $indices[$ind];
      $word = $mywords[$number-1];
      echo "<li class=\"list-group-item\"><span class=\"badge badge-light\">$number</span> $word</li>";
    }
    echo '</ul>';
  };

  $plainWordList = function($words) {
    echo '<ul class="list-group list-group-horizontal lg-lesspadding">';
    foreach ($words as $word) {
      echo "<li class=\"list-group-item\">$word</li>";
    }
    echo '</ul>';
  };

?>

<html>
  <head>
    <?php htmlHead(); ?>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script> -->
    <title>Decryptio</title>
  </head>
  <body>
    <?php // $showScores(); ?>
    <div class="container">
<?php
    // echo "<p>You are <b>$playername.</b></p>";

    // show the scores

    echo "you are $playername. <br> you are on team $myteamname.<br>";

    echo "your team's words: "; $wordList();

    // show the dilemma
    if ($mystate == STATE_ENCRYPTING) {
      if ($encryptorisme) {
        echo "you are the encryptor. you must encrypt the message ${mymessage[message]} <br>";
        $encryptForm();
      } else {
        echo "you are waiting for $myencryptorname to encrypt.";
        $refreshonchange = true;
      }
    } else if ($mystate == STATE_DECRYPTING) {
      if ($encryptorisme) {
        echo "the message is: ";
        $wordList(json_decode($mymessage['message']));
        echo "your encription: ";
        $plainWordList(json_decode($mymessage['encrypted']));
        echo "you must wait for your team to decrypt your message.";
        $refreshonchange = true;
      } else {
        echo "$myencryptorname has encrypted the message and given you the following:";
        $plainWordList(json_decode($mymessage['encrypted']));
        $decryptForm();
      }
    } else if ($mystate == STATE_INTERCEPTING) {
      if (
          (INCERCEPT_BEFORE_DECRYPTED && $theirstate == STATE_DECRYPTING) // they haven't decrypted yet (if the config allows this).
        || $theirstate == STATE_INTERCEPTING // they have finished decrypting, and are intercepting you.
        || $theirstate == STATE_RESULTS) // they have finished intercepting you and are waiting.
      {
        $theirmessage = getMessage($session, $theirteam);
        $theirencryption = json_decode($theirmessage['encrypted']);
        $theirhistory = clueHistory($sessionid, $theirteam, $theirmessage['id']);

        echo "Here are the previous clues used by the enemy team.";
        $showHistory($theirhistory);
        echo "Try to intercept their message by matching their encrypted message to these groups.";
        $interceptForm($theirencryption);
        $interceptwait = true;
      } else {
        // WAITING FOR THE OTHER TEAM BEFORE WE INTERCEPT
        if ($encryptorisme) {
          echo "the message is: ";
          $wordList(json_decode($mymessage['message']));
          echo "your encription: ";
          $plainWordList(json_decode($mymessage['encrypted']));
          echo "your team has decrypted the message. waiting on the other team to do so.";
          $refreshonchange = true;
        } else {
          echo "$myencryptorname has encrypted the message and given you the following:";
          $plainWordList(json_decode($mymessage['encrypted']));
          echo "you decrypted the message into the following:";
          $wordList(json_decode($mymessage['decrypted']));
          echo "waiting on the other team to decrypt.";
          $refreshonchange = true;
        }
      }
    } else if ($mystate == STATE_RESULTS) {
      echo "you are waiting on the other team.";
      $refreshonchange = true;

    } else {
      echo "the game is in state $mystate, which is not yet implemented.";
      $refreshonchange = true;
    }
?>
    </div>
    <script type="text/javascript">

      function checkDecrypt() {
        var selects = $("select.decryptselect");
        var used = new Set();
        var repeats = false;
        selects.each(function(){
          var thisval = $(this).val();
          if (used.has(thisval)) repeats = true;
          used.add(thisval);
        });
        if (repeats) {
          alert("The message does not repeat itself.");
          return false;
        }
        return true;
      }

      // jquery 'onload'
      $(function() { <?php
        
        if ($refreshonchange)
          $refreshBehaviour();
        
        if ($interceptwait)
          $interceptWaitBehaviour();

        
      ?> });
    </script>
  </body>
</html>