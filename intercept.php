<?php
  require_once 'db/common.php';

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // get the session, and make sure it's all good
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);
  dieIfNotPhase($session, PHASE_PLAYING);
  
  // team details
  $myteam = getPlayerTeam($sessionid, $playerid);
  $myteamname = teamName($myteam);
  $mystate = $session["${myteamname}state"];

  // check we are in right state and are the right player
  if ($mystate != STATE_INTERCEPTING) error("game is not in correct state.");

  $guesses = array();
  for ($index=1; $index<=GUESS_COUNT; $index++) {
    $guess = $_POST["intercept_$index"];
    array_push($guesses, intval($guess));
  }

  $guessesjson = json_encode($guesses);

  // record the interception attempt and update the game state.
	submitInterception($session, $myteam, $guessesjson);
	
	// if both teams are in results, move to the next round
  if (bothTeamsInResults($sessionid)) {
		newRound($sessionid);
  }

?>
<html>
   <head>
      <title>Decryptio</title>
      <meta http-equiv = 'refresh' content = '0; url = .?<?=$_SERVER['QUERY_STRING']?>' />
   </head>
   <body>
      <p>Decrypting message...</p>
   </body>
</html>