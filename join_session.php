<?php
  require 'db/common.php';

  $playername = $_REQUEST['name'];
  $sessionid = $_REQUEST['session'];
  $sessionpass = $_REQUEST['pass'];

  // make sure we have a valid session
  $session = getSession($sessionid, $sessionpass);
  dieIfInvalidSession($session);
  if ($session['phase'] != PHASE_NOTSTARTED)
    die('session is closed');

  $playerid = createPlayer($playername, $sessionid);

  $URLInfo = "?session=$sessionid&pass=$sessionpass&player=$playerid";

?>
<html>
   <head>
      <title>Joining session</title>
      <meta http-equiv = 'refresh' content = '0; url = waiting_room.php<?=$URLInfo?>' />
   </head>
   <body>
      <p>Joining new session...</p>
   </body>
</html>